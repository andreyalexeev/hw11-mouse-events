"use strict";

// task 1

const button = document.getElementById('btn-click');
const section = document.getElementById('content');

button.addEventListener('click', function() {

const newParagraph = document.createElement('p');
newParagraph.textContent = 'New Paragraph'; 

section.appendChild(newParagraph);
});

// task 2

// const section = document.getElementById('content');
// const footer = document.querySelector('footer');

// const createButton = document.getElementById('btn-input-create');
// createButton.addEventListener('click', function() {

// const newInput = document.createElement('input');
// newInput.type = 'text'; 
// newInput.placeholder = 'Введіть текст...';
// newInput.name = 'myInput'; 

// footer.appendChild(newInput);
// });